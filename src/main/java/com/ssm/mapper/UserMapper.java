package com.ssm.mapper;

import java.util.List;

import com.ssm.pojo.User;

public interface UserMapper {

	List<User> findUser();
	
}
