package com.ssm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ssm.pojo.User;
import com.ssm.service.UserService;



@Controller
public class PageController {
	
	@Autowired
	private UserService userServicce;
	

	/**
	 * 访问页面
	 * TODO:
	 * @return:
	 * @author:GaoJianLin 13995949217@163.com
	 * 2018年4月28日下午3:15:55
	 */
	
	@RequestMapping("{page}")
	public String showPage(@PathVariable("page") String page){
		return page;
	}
	
	
	@RequestMapping("sel")
	public String userInfo(Model model){
		System.out.println("依赖的service对象-"+userServicce);
		List<User> selById = userServicce.selById();
		for (User user : selById) {
			System.out.println(user);
		}
		model.addAttribute("user", selById);
		return "main";
	}
	
	
	@RequestMapping("/option")
	public ModelAndView getOpt(String sex){
		ModelAndView mav = new ModelAndView();
		System.out.println(sex);
		List<User> selById = userServicce.selById();
		System.out.println(selById);
		mav.setViewName("main");
		mav.addObject("ulist", selById);
		return mav;
	}
}
